### Description
The only difference between this example and the [KnownLengthArray](Cxx/Utilities/KnownLengthArray) example is that `SetNumberOfValues()` is not called, and `SetValue()` is replaced by `InsertNextValue()`.
