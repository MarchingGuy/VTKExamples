### Description
This example demonstrates how to apply a transform to a data set. It uses {{class|vtkTransformPolyDataFilter}}, but it can be replaced with {{class|vtkTransformFilter}} for different types of data sets, including {{class|vtkUnstructuredGrid}} and {{class|vtkStructuredGrid}}. ({{class|vtkTransformFilter}} will work with {{class|vtkPolyData}}, too).
