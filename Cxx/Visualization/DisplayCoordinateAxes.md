### Description
This example shows how to display the coordinate axes in the render window.

**See also:** [Axes](Cxx/GeometricObjects/Axes).
